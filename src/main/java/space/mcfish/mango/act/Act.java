package space.mcfish.mango.act;

import java.awt.Graphics2D;

public interface Act {
    public void serialize(StringBuilder sb);

    public void enflate(String serialized);

    public void perform(Graphics2D g);

    public void rescaleX(float xRatio);

    public void rescaleY(float yRatio);

    public void rotateAround(float x, float y, float degrees);
}
