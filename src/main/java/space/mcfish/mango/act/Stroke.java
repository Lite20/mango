package space.mcfish.mango.act;

import java.awt.Graphics2D;
import java.awt.geom.Path2D;

import org.json.JSONArray;

public class Stroke implements Act {

    private final Point[] points;

    private final Path2D strokePath;

    public Stroke(Point[] p) {
        points = p;
        strokePath = new Path2D.Float();
        buildPath();
    }

    public Stroke(JSONArray actData) {
        JSONArray pointArray = actData.getJSONArray(1);
        int length = pointArray.length();

        points = new Point[length];

        for (int i = 0; i < length; i++)
            points[i] = new Point(pointArray.getJSONArray(i));

        strokePath = new Path2D.Float();
        buildPath();
    }

    public void serialize(StringBuilder sb) {
        sb.append("[\"stroke\",[");

        for (int i = 0; i < points.length; i++) {
            points[i].serialize(sb);
            if (i != points.length - 1)
                sb.append(',');
        }

        sb.append("]]");
    }

    public void buildPath() {
        strokePath.moveTo(points[0].x, points[0].y);
        for (int i = 1; i < points.length; i++)
            strokePath.lineTo(points[i].x, points[i].y);
    }

    public void enflate(String serialized) {
        // TODO Auto-generated method stub

    }

    public void perform(Graphics2D g) {
        g.draw(strokePath);
    }

    public void rescaleX(float xRatio) {
        // TODO Auto-generated method stub

    }

    public void rescaleY(float yRatio) {
        // TODO Auto-generated method stub

    }

    public void rotateAround(float x, float y, float degrees) {
        // TODO Auto-generated method stub

    }
}
