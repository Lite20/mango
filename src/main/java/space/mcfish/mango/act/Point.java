package space.mcfish.mango.act;

import org.json.JSONArray;

public class Point {
    public float x;
    public float y;

    public Point(float x, float y) {
        this.x = x;
        this.y = y;
    }

    public Point(JSONArray pos) {
        x = pos.getFloat(0);
        y = pos.getFloat(1);
    }

    public void serialize(StringBuilder sb) {
        sb.append('[');
        sb.append(x);
        sb.append(',');
        sb.append(y);
        sb.append(']');
    }
}
