package space.mcfish.mango.events;

import java.awt.Point;
import java.awt.event.ComponentEvent;
import java.awt.event.ComponentListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

import jwinpointer.JWinPointerReader.PointerEventListener;
import space.mcfish.mango.Core;
import space.mcfish.mango.ui.Renderer;
import space.mcfish.mango.ui.Renderer.View;
import space.mcfish.mango.ui.component.window.Window;
import space.mcfish.mango.ui.util.UI;
import space.mcfish.mango.ui.view.MenuView;

public class EventHandler implements PointerEventListener, KeyListener, ComponentListener {
    public void componentHidden(ComponentEvent e) {
        // TODO Auto-generated method stub

    }

    public void componentMoved(ComponentEvent e) {
        // TODO Auto-generated method stub

    }

    public void componentResized(ComponentEvent e) {
        UI.recomputeSize();
    }

    public void componentShown(ComponentEvent e) {

    }

    public void keyPressed(KeyEvent e) {
        if (e.getKeyCode() == KeyEvent.VK_ESCAPE)
            System.exit(0);
    }

    public void keyReleased(KeyEvent e) {
        // TODO Auto-generated method stub

    }

    public void keyTyped(KeyEvent e) {
        // TODO Auto-generated method stub

    }

    public void pointerButtonEvent(int deviceType, int pointerID, int type, boolean inverted, int buttonIndex) {

    }

    public void pointerEvent(int deviceType, int pointerID, int type, boolean inverted) {

    }

    public void pointerXYEvent(int deviceType, int pointerID, int eType, boolean inverted, int x, int y, int pressure) {
        y -= Core.yPad;
        if (Renderer.currentView == View.MENU) {
            MenuView.onPointer(eType, x, y);
        } else if (Renderer.currentView == View.WORKSPACE) {
            for (Window win : Renderer.windows) {
                if (win.getBounds().contains(new Point(x, y))) {
                    win.onPointer(deviceType, pointerID, eType, inverted, x, y, pressure);
                    break;
                }
            }
        } else if (Renderer.currentView == View.PANE) {
            if (eType == PointerEvent.EVENT_TYPE_DRAG && Renderer.dragging) {
                if (Core.getCurrentPage().lastX == 0 && Core.getCurrentPage().lastY == 0) {
                    Core.getCurrentPage().lastX = x;
                    Core.getCurrentPage().lastY = y;
                } else {
                    Core.getCurrentPage().xScrollOffset += x - Core.getCurrentPage().lastX;
                    Core.getCurrentPage().yScrollOffset += y - Core.getCurrentPage().lastY;
                    Core.getCurrentPage().lastX = x;
                    Core.getCurrentPage().lastY = y;
                }
            } else if (eType == PointerEvent.EVENT_TYPE_DRAG)
                Core.getCurrentPage().getActivePane().moveStroke(
                        (int) ((x - Core.getCurrentPage().xScrollOffset) / Core.getCurrentPage().scale),
                        (int) ((y - Core.getCurrentPage().yScrollOffset) / Core.getCurrentPage().scale));
            else if (eType == PointerEvent.EVENT_TYPE_DOWN && !Renderer.dragging)
                Core.getCurrentPage().getActivePane().startStroke(
                        (int) ((x - Core.getCurrentPage().xScrollOffset) / Core.getCurrentPage().scale),
                        (int) ((y - Core.getCurrentPage().yScrollOffset) / Core.getCurrentPage().scale));
            else if (eType == PointerEvent.EVENT_TYPE_UP) {
                Core.getCurrentPage().getActivePane().endStroke(
                        (int) ((x - Core.getCurrentPage().xScrollOffset) / Core.getCurrentPage().scale),
                        (int) ((y - Core.getCurrentPage().yScrollOffset) / Core.getCurrentPage().scale));
                Core.getCurrentPage().dirty = true;
                Core.getCurrentPage().lastX = 0;
                Core.getCurrentPage().lastY = 0;
            }
        }
    }
}
