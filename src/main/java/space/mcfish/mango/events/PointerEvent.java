package space.mcfish.mango.events;

public class PointerEvent {
    public static final int EVENT_TYPE_DRAG = 1;
    public static final int EVENT_TYPE_HOVER = 2;
    public static final int EVENT_TYPE_DOWN = 3;
    public static final int EVENT_TYPE_UP = 4;
    public static final int EVENT_TYPE_BUTTON_DOWN = 5;
    public static final int EVENT_TYPE_BUTTON_UP = 6;
    public static final int EVENT_TYPE_IN_RANGE = 7;
    public static final int EVENT_TYPE_OUT_OF_RANGE = 8;
}
