package space.mcfish.mango;

import java.io.File;
import java.net.URISyntaxException;

import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.UIManager;
import javax.swing.UnsupportedLookAndFeelException;
import javax.swing.filechooser.FileNameExtensionFilter;
import javax.swing.filechooser.FileSystemView;

import jwinpointer.JWinPointerReader;
import space.mcfish.mango.doc.Page;
import space.mcfish.mango.events.EventHandler;
import space.mcfish.mango.ui.LoadingScreen;
import space.mcfish.mango.ui.Renderer;
import space.mcfish.mango.ui.Renderer.View;
import space.mcfish.mango.ui.Resources;
import space.mcfish.mango.ui.component.window.PanelWindow;
import space.mcfish.mango.ui.util.DInt;
import space.mcfish.mango.ui.util.DRect;
import space.mcfish.mango.ui.util.UI;

public class Core {
    public static final String VERSION = "0.2.2";

    public static JFrame lw;

    private static EventHandler eHandle;

    private static Page page;

    public static boolean running = true;

    public static JFrame w;

    public static JWinPointerReader pointer;

    public static Renderer p;

    public static LoadingScreen lp;

    public static Resources res;

    public static int yPad = 0;

    // TODO use real variable names
    public static void main(String args[]) {
        // *** INITIALIZATION PHASE ***
        // create event handler
        eHandle = new EventHandler();

        // create panels (one for loading)
        p = new Renderer();
        lp = new LoadingScreen();

        // construct loading window with loading pane
        lw = UI.createLoader(lp);

        // show loading window
        lw.setVisible(true);

        // *** LOADING PHASE ***
        // load resources
        try {
            UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
            res = new Resources();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        } catch (InstantiationException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (UnsupportedLookAndFeelException e) {
            e.printStackTrace();
        } catch (URISyntaxException e) {
            e.printStackTrace();
        }

        // build main editor window with main pane and event handler
        w = UI.createEditor(p, eHandle, false);

        // signal UI to recompute its dimensions
        UI.recomputeSize();

        // instantiate pointer reader on main editor window
        pointer = new JWinPointerReader(w);
        pointer.addPointerEventListener(eHandle);

        // *** RUNTIME PHASE ***
        // remove loading window and bring up editor window
        lw.setVisible(false);
        lp.loading = false;
        lw.dispose();

        w.setVisible(true);
    }

    public static void showPageOpenDialog() {
        JFileChooser jfc = getChooser();
        if (jfc.showOpenDialog(null) == JFileChooser.APPROVE_OPTION) {
            w.setTitle("Mango - " + jfc.getSelectedFile().getPath());
            page = new Page(jfc.getSelectedFile(), true);
            moveToWorkspace();
        }
    }

    public static void showPageCreationDialog() {
        JFileChooser jfc = getChooser();
        if (jfc.showSaveDialog(null) == JFileChooser.APPROVE_OPTION) {
            if (!jfc.getSelectedFile().getAbsolutePath().endsWith(".mango"))
                jfc.setSelectedFile(new File(jfc.getSelectedFile().getAbsolutePath() + ".mango"));

            page = new Page(jfc.getSelectedFile(), false);
            moveToWorkspace();
        }
    }

    public static JFileChooser getChooser() {
        JFileChooser jfc = new JFileChooser(FileSystemView.getFileSystemView().getHomeDirectory());
        jfc.addChoosableFileFilter(new FileNameExtensionFilter("Mango Files", "mango"));
        jfc.setAcceptAllFileFilterUsed(false);
        jfc.setFileSelectionMode(JFileChooser.FILES_ONLY);
        return jfc;
    }

    public static void moveToWorkspace() {
        Renderer.currentView = View.WORKSPACE;
        Renderer.windows.add(new PanelWindow(new DRect(new DInt(0, DInt.X_AXIS), new DInt(0, DInt.Y_AXIS),
                new DInt(1, DInt.X_AXIS), new DInt(1, DInt.Y_AXIS))));
    }

    public static Page getCurrentPage() {
        return page;
    }
}
