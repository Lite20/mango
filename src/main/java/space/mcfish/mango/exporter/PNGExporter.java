package space.mcfish.mango.exporter;

import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;
import javax.swing.JFileChooser;
import javax.swing.filechooser.FileNameExtensionFilter;
import javax.swing.filechooser.FileSystemView;

import space.mcfish.mango.doc.Page;

public class PNGExporter {

    public static void export(Page page) throws IOException {
        JFileChooser jfc = new JFileChooser(FileSystemView.getFileSystemView().getHomeDirectory());
        jfc.addChoosableFileFilter(new FileNameExtensionFilter("PNG Image", "png"));
        jfc.setFileSelectionMode(JFileChooser.FILES_ONLY);
        if (jfc.showSaveDialog(null) != JFileChooser.APPROVE_OPTION)
            return;

        int w = 0;
        int h = 0;

        for (int i = 0; i < page.panes.size(); i++) {
            w = (int) Math.max(w, page.panes.get(i).x + page.panes.get(i).w);
            h = (int) Math.max(h, page.panes.get(i).y + page.panes.get(i).h);
        }

        BufferedImage buff = new BufferedImage(w, h, BufferedImage.TYPE_INT_RGB);
        Graphics2D g = buff.createGraphics();

        g.setColor(Color.WHITE);
        g.fillRect(0, 0, w, h);

        for (int i = 0; i < page.panes.size(); i++) {
            page.panes.get(i).paint();
            g.drawImage(page.panes.get(i).buffer, (int) page.panes.get(i).x, (int) page.panes.get(i).y,
                    (int) page.panes.get(i).w, (int) page.panes.get(i).h, null);
        }

        if (!jfc.getSelectedFile().getAbsolutePath().endsWith(".png"))
            jfc.setSelectedFile(new File(jfc.getSelectedFile().getAbsolutePath() + ".png"));

        ImageIO.write(buff, "png", jfc.getSelectedFile());
    }
}
