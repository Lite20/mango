package space.mcfish.mango.exporter;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

import javax.swing.JFileChooser;
import javax.swing.filechooser.FileNameExtensionFilter;
import javax.swing.filechooser.FileSystemView;

import org.jfree.graphics2d.svg.SVGGraphics2D;

import space.mcfish.mango.doc.Page;

public class SVGExporter {

    public static void export(Page page) throws IOException {
        JFileChooser jfc = new JFileChooser(FileSystemView.getFileSystemView().getHomeDirectory());
        jfc.addChoosableFileFilter(new FileNameExtensionFilter("PNG Image", "png"));
        jfc.setFileSelectionMode(JFileChooser.FILES_ONLY);
        if (jfc.showSaveDialog(null) != JFileChooser.APPROVE_OPTION)
            return;

        int w = 0;
        int h = 0;

        for (int i = 0; i < page.panes.size(); i++) {
            w = (int) Math.max(w, page.panes.get(i).x + page.panes.get(i).w);
            h = (int) Math.max(h, page.panes.get(i).y + page.panes.get(i).h);
        }

        SVGGraphics2D g = new SVGGraphics2D(w, h);
        for (int i = 0; i < page.panes.size(); i++)
            page.panes.get(i).svgPaint(g);

        if (!jfc.getSelectedFile().getAbsolutePath().endsWith(".svg"))
            jfc.setSelectedFile(new File(jfc.getSelectedFile().getAbsolutePath() + ".svg"));

        BufferedWriter writer = new BufferedWriter(new FileWriter(jfc.getSelectedFile()));
        writer.write(g.getSVGElement());
        writer.close();
    }
}
