package space.mcfish.mango.doc;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.RenderingHints;
import java.awt.image.BufferedImage;
import java.util.ArrayList;
import java.util.List;

import org.jfree.graphics2d.svg.SVGGraphics2D;
import org.json.JSONArray;
import org.json.JSONObject;

import space.mcfish.mango.act.Act;
import space.mcfish.mango.act.Point;
import space.mcfish.mango.act.Stroke;

public class Pane {
    public float x;
    public float y;
    public float w;
    public float h;

    public ArrayList<Point> liveLine = new ArrayList<Point>(3);

    public List<Act> acts = new ArrayList<Act>();

    public BufferedImage buffer;

    public Graphics2D g;

    private java.awt.Stroke stroke = new BasicStroke(3);

    private int lastX = 0;
    private int lastY = 0;

    public Pane(float x, float y, float width, float height) {
        this.x = x;
        this.y = y;
        this.w = width;
        this.h = height;

        buffer = new BufferedImage((int) width, (int) height, BufferedImage.TYPE_INT_RGB);
        resetBuffer();
    }

    public Pane(JSONObject paneData) {
        this.x = paneData.getFloat("x");
        this.y = paneData.getFloat("y");
        this.w = paneData.getFloat("w");
        this.h = paneData.getFloat("h");

        buffer = new BufferedImage((int) w, (int) h, BufferedImage.TYPE_INT_RGB);
        resetBuffer();

        JSONArray actData = paneData.getJSONArray("acts");
        for (int i = 0; i < actData.length(); i++) {
            if (actData.getJSONArray(i).getString(0).equals("stroke"))
                acts.add(new Stroke(actData.getJSONArray(i)));
        }
    }

    public Pane(Pane pane) {
        this.x = pane.x;
        this.y = pane.y;
        this.w = pane.w;
        this.h = pane.h;

        buffer = new BufferedImage((int) w, (int) h, BufferedImage.TYPE_INT_RGB);
        resetBuffer();
    }

    /**
     * Serialize a pane's current actions as JSON.
     * 
     * @param sb
     *            The string builder to append the JSON to.
     */
    public void serialize(StringBuilder sb) {
        sb.append("{\"w\":" + w + ",\"h\":" + h + ",\"x\":" + x + ",\"y\":" + y + ",\"acts\":[");

        for (int i = 0; i < acts.size(); i++) {
            acts.get(i).serialize(sb);
            if (i != acts.size() - 1)
                sb.append(",");
        }

        sb.append("]}");
    }

    public void paint() {
        paint(acts);
    }

    /**
     * Executes a sequence of actions onto the pane's canvas.
     * 
     * @param actList
     *            The list of actions to perform.
     */
    public void paint(List<Act> actList) {
        g.setColor(Color.WHITE);
        g.fillRect(0, 0, (int) w, (int) h);
        g.setStroke(stroke);
        g.setColor(Color.BLACK);
        for (Act act : actList)
            act.perform(g);
    }

    public void resizeX(float xRatio) {
        g.dispose();
        buffer.flush();
        buffer = new BufferedImage((int) (w * xRatio), (int) h, BufferedImage.TYPE_INT_RGB);
        resetBuffer();
    }

    public void resizeY(float yRatio) {
        g.dispose();
        buffer.flush();
        buffer = new BufferedImage((int) w, (int) (h * yRatio), BufferedImage.TYPE_INT_RGB);
        resetBuffer();
    }

    public void rescaleX(float xRatio) {
        for (Act act : acts)
            act.rescaleX(xRatio);
    }

    public void rescaleY(float yRatio) {
        for (Act act : acts)
            act.rescaleY(yRatio);
    }

    public void rotateAround(float x, float y, float degrees) {
        for (Act act : acts)
            act.rotateAround(x, y, degrees);
    }

    public void startStroke(int x2, int y2) {
        liveLine.add(new Point(x2 - x, y2 - y));
        lastX = x2;
        lastY = y2;
    }

    public void moveStroke(int x2, int y2) {
        if (Math.hypot(x2 - lastX, y2 - lastY) > 2) {
            lastX = x2;
            lastY = y2;
            liveLine.add(new Point(x2 - x, y2 - y));
        }
    }

    public void endStroke(int x2, int y2) {
        if (Math.hypot(x2 - lastX, y2 - lastY) > 4)
            liveLine.add(new Point(x2 - x, y2 - y));

        Point[] points = new Point[liveLine.size()];
        liveLine.toArray(points);
        acts.add(new Stroke(points));

        paint();
        liveLine = new ArrayList<Point>(3);
    }

    /**
     * Set/reset the pane's buffer for drawing to. Also sets rendering hints and
     * background color to white.
     */
    public void resetBuffer() {
        g = (Graphics2D) buffer.getGraphics();
        g.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
        g.setRenderingHint(RenderingHints.KEY_STROKE_CONTROL, RenderingHints.VALUE_STROKE_PURE);
        g.setColor(Color.WHITE);
        g.fillRect(0, 0, (int) w, (int) h);
    }

    /**
     * Paints contents of pane using a SVGGraphics2D object instead of
     * {@link Graphics2D} (to render to a svg file).
     * 
     * @param g
     *            The SVGGraphics2D object to use.
     */
    public void svgPaint(SVGGraphics2D g) {
        g.setStroke(stroke);
        g.setColor(Color.BLACK);
        for (Act act : acts)
            act.perform(g);
    }
}
