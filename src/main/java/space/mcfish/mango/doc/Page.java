package space.mcfish.mango.doc;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Timer;

import org.json.JSONArray;

import space.mcfish.mango.SaveThread;

public class Page {
    public ArrayList<Pane> panes;

    public File saveFile;

    public int activePaneIndex = 0;

    public boolean dirty = false;

    // TODO wrap the following 5 variables in some form of transform object
    public int xScrollOffset = 0;
    public int yScrollOffset = 0;

    public int lastX = 0;
    public int lastY = 0;

    public float scale;

    public Page(File f, boolean load) {
        panes = new ArrayList<Pane>();
        saveFile = f;
        try {
            if (load)
                load();
        } catch (IOException e) {
            e.printStackTrace();
        }

        Timer timer = new Timer();
        timer.schedule(new SaveThread(), 0, 1000);
    }

    public Pane getActivePane() {
        return panes.get(activePaneIndex);
    }

    public void serialize(StringBuilder sb) {
        sb.append('[');
        for (int i = 0; i < panes.size(); i++) {
            panes.get(i).serialize(sb);
            if (i != panes.size() - 1)
                sb.append(',');
        }

        sb.append(']');
    }

    /**
     * Loads a page from it's save file
     * 
     * @throws IOException
     */
    public void load() throws IOException {
        Pane pane;
        byte[] encoded = Files.readAllBytes(Paths.get(saveFile.getAbsolutePath()));
        String data = new String(encoded, StandardCharsets.UTF_8.name());
        JSONArray obj = new JSONArray(data);
        panes = new ArrayList<Pane>(obj.length());
        for (int i = 0; i < obj.length(); i++) {
            pane = new Pane(obj.getJSONObject(i));
            panes.add(pane);
            pane.paint();
        }
    }

    public void save() {
        dirty = false;
        try {
            StringBuilder data = new StringBuilder();
            FileWriter f = new FileWriter(saveFile, false);
            serialize(data);
            f.write(data.toString());
            f.flush();
            f.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void undo() {
        getActivePane().acts.remove(getActivePane().acts.size() - 1);
    }

    public int paneCount() {
        return panes.size();
    }
}
