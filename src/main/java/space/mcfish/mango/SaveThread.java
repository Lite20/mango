package space.mcfish.mango;

import java.util.TimerTask;

public class SaveThread extends TimerTask {

    @Override
    public void run() {
        if (Core.getCurrentPage().dirty)
            Core.getCurrentPage().save();
    }
}
