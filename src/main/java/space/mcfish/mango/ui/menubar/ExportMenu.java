package space.mcfish.mango.ui.menubar;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.io.IOException;

import javax.swing.JMenu;
import javax.swing.JMenuItem;

import space.mcfish.mango.Core;
import space.mcfish.mango.exporter.PNGExporter;
import space.mcfish.mango.exporter.SVGExporter;

public class ExportMenu {

    public static void bake(JMenu menu) {
        JMenu exportMenu = new JMenu("Export");

        JMenuItem pngExportItem = new JMenuItem("As PNG", KeyEvent.VK_P);
        JMenuItem svgExportItem = new JMenuItem("As SVG", KeyEvent.VK_V);

        exportMenu.setMnemonic(KeyEvent.VK_E);
        exportMenu.getAccessibleContext().setAccessibleDescription("Export Menu");

        pngExportItem.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                try {
                    PNGExporter.export(Core.getCurrentPage());
                } catch (IOException e1) {
                    e1.printStackTrace();
                }
            }
        });

        svgExportItem.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                try {
                    SVGExporter.export(Core.getCurrentPage());
                } catch (IOException e1) {
                    e1.printStackTrace();
                }
            }
        });

        exportMenu.add(pngExportItem);
        exportMenu.add(svgExportItem);

        menu.add(exportMenu);
    }

}
