package space.mcfish.mango.ui.menubar;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;

import javax.swing.JFileChooser;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.filechooser.FileNameExtensionFilter;
import javax.swing.filechooser.FileSystemView;

import space.mcfish.mango.Core;

public class FileMenu {

    public static void bake(JMenuBar menu) {
        JMenu fileMenu = new JMenu("File");

        JMenuItem newItem = new JMenuItem("New", KeyEvent.VK_N);
        JMenuItem openItem = new JMenuItem("Open", KeyEvent.VK_O);
        JMenuItem saveAsItem = new JMenuItem("Save As", KeyEvent.VK_S);
        JMenuItem netItem = new JMenuItem("Network", KeyEvent.VK_T);
        JMenuItem exitItem = new JMenuItem("Exit", KeyEvent.VK_X);

        fileMenu.setMnemonic(KeyEvent.VK_F);
        fileMenu.getAccessibleContext().setAccessibleDescription("File Menu");

        newItem.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                Core.showPageCreationDialog();
            }
        });

        openItem.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                Core.showPageOpenDialog();
            }
        });

        saveAsItem.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                JFileChooser jfc = new JFileChooser(FileSystemView.getFileSystemView().getHomeDirectory());
                jfc.addChoosableFileFilter(new FileNameExtensionFilter("Mango Files", "mango"));
                jfc.setAcceptAllFileFilterUsed(false);
                jfc.setFileSelectionMode(JFileChooser.FILES_ONLY);
                if (jfc.showSaveDialog(null) == JFileChooser.APPROVE_OPTION)
                    Core.getCurrentPage().saveFile = jfc.getSelectedFile();
            }
        });

        exitItem.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                System.exit(0);
            }
        });

        fileMenu.add(newItem);
        fileMenu.add(openItem);
        fileMenu.add(saveAsItem);
        fileMenu.addSeparator();
        ExportMenu.bake(fileMenu);
        fileMenu.addSeparator();
        fileMenu.add(netItem);
        fileMenu.addSeparator();
        fileMenu.add(exitItem);

        menu.add(fileMenu);
    }
}
