package space.mcfish.mango.ui.menubar;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;

import javax.swing.JMenu;
import javax.swing.JMenuItem;

import space.mcfish.mango.Core;
import space.mcfish.mango.doc.Pane;

public class PaneMenu {

    public static void bake(JMenu menu) {
        JMenu paneMenu = new JMenu("Pane");
        JMenuItem addPaneItem = new JMenuItem("Add", KeyEvent.VK_A);
        JMenuItem clonePaneItem = new JMenuItem("Clone", KeyEvent.VK_C);
        JMenuItem removePaneItem = new JMenuItem("Remove", KeyEvent.VK_R);

        paneMenu.setMnemonic(KeyEvent.VK_P);
        paneMenu.getAccessibleContext().setAccessibleDescription("Pane Menu");

        addPaneItem.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                Core.getCurrentPage().panes.add(new Pane(0, 0, 100, 100));
            }
        });

        clonePaneItem.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                Pane p = new Pane(Core.getCurrentPage().getActivePane());
                p.x += p.w;
                Core.getCurrentPage().panes.add(p);
            }
        });

        removePaneItem.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                Core.getCurrentPage().panes.remove(Core.getCurrentPage().activePaneIndex);
            }
        });

        paneMenu.add(addPaneItem);
        paneMenu.add(clonePaneItem);
        paneMenu.add(removePaneItem);

        menu.add(paneMenu);
    }

}
