package space.mcfish.mango.ui.menubar;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;

import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;

import space.mcfish.mango.Core;

public class HelpMenu {

    public static void bake(JMenuBar menu) {
        JMenu helpMenu = new JMenu("Help");
        JMenuItem aboutItem = new JMenuItem("About", KeyEvent.VK_A);

        helpMenu.setMnemonic(KeyEvent.VK_H);
        helpMenu.getAccessibleContext().setAccessibleDescription("Help Menu");

        aboutItem.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                JOptionPane.showMessageDialog(Core.w, "Mango v" + Core.VERSION + " by @lite.mcfish (on twitter)",
                        "About", JOptionPane.INFORMATION_MESSAGE);
            }
        });

        helpMenu.add(aboutItem);

        menu.add(helpMenu);
    }
}
