package space.mcfish.mango.ui.menubar;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;

import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;

import space.mcfish.mango.Core;

public class EditMenu {

    public static void bake(JMenuBar menu) {
        JMenu editMenu = new JMenu("Edit");
        JMenuItem undoItem = new JMenuItem("Undo", KeyEvent.VK_U);
        JMenuItem redoItem = new JMenuItem("Redo", KeyEvent.VK_R);

        editMenu.setMnemonic(KeyEvent.VK_E);
        editMenu.getAccessibleContext().setAccessibleDescription("Edit Menu");

        undoItem.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                Core.getCurrentPage().undo();
            }
        });

        editMenu.add(undoItem);
        editMenu.add(redoItem);
        editMenu.addSeparator();
        PaneMenu.bake(editMenu);

        menu.add(editMenu);
    }
}
