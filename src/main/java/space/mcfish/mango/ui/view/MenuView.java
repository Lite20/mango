package space.mcfish.mango.ui.view;

import java.awt.Color;
import java.awt.Graphics2D;

import space.mcfish.mango.Core;
import space.mcfish.mango.events.PointerEvent;
import space.mcfish.mango.ui.Resources;
import space.mcfish.mango.ui.util.UI;

public class MenuView {

    public static void draw(Graphics2D g, int WIDTH, int HEIGHT) {
        g.setColor(UI.dullColor);
        g.fillRect(0, 0, WIDTH, HEIGHT);
        g.setColor(Color.WHITE);
        g.fillRect(2, 2, (WIDTH / 2) - 4, HEIGHT - 4);
        g.fillRect((WIDTH / 2) + 2, 2, (WIDTH / 2) - 4, HEIGHT - 4);
        int dim = (WIDTH / 10);
        g.drawImage(Resources.OPEN_FILE, (WIDTH / 4) - dim, (HEIGHT / 2) - dim, (dim * 2), (dim * 2), null);
        g.drawImage(Resources.NEW_FILE, ((WIDTH / 4) * 3) - dim, (HEIGHT / 2) - dim, (dim * 2), (dim * 2), null);
    }

    public static void onPointer(int eType, int x, int y) {
        if (eType == PointerEvent.EVENT_TYPE_DOWN) {
            if (x < Core.w.getWidth() / 2)
                Core.showPageOpenDialog();
            else
                Core.showPageCreationDialog();
        }
    }
}
