package space.mcfish.mango.ui.view;

import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.geom.Path2D;

import space.mcfish.mango.Core;
import space.mcfish.mango.doc.Page;
import space.mcfish.mango.doc.Pane;
import space.mcfish.mango.ui.util.UI;

public class PaneView {
    public static void draw(Graphics2D g, int WIDTH, int HEIGHT) {
        final Page page = Core.getCurrentPage();
        final Pane pane = page.getActivePane();
        g.setColor(UI.dullColor);
        g.fillRect(0, 0, WIDTH, HEIGHT);
        g.setColor(UI.lineColor);
        g.setStroke(UI.borderStroke);
        g.drawImage(pane.buffer, (int) ((page.xScrollOffset + pane.x) * page.scale),
                (int) ((page.yScrollOffset + pane.y) * page.scale), (int) (pane.w * page.scale),
                (int) (pane.h * page.scale), null);
        g.drawRect((int) ((page.xScrollOffset + pane.x) * page.scale),
                (int) ((page.yScrollOffset + pane.y) * page.scale), (int) (pane.w * page.scale),
                (int) (pane.h * page.scale));

        if (pane.liveLine.size() > 0) {
            g.setStroke(UI.stroke);
            g.setColor(Color.RED);

            Path2D p = new Path2D.Float();

            float x, y;
            for (int i = 0; i < pane.liveLine.size(); i++) {
                x = page.xScrollOffset + pane.liveLine.get(i).x + Core.getCurrentPage().getActivePane().x;
                y = page.yScrollOffset + pane.liveLine.get(i).y + Core.getCurrentPage().getActivePane().y;

                if (i == 0)
                    p.moveTo(page.scale * x, page.scale * y);

                else
                    p.lineTo(page.scale * x, page.scale * y);
            }

            g.draw(p);
        }
    }
}
