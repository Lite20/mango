package space.mcfish.mango.ui;

import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.RenderingHints;
import java.util.ArrayList;

import javax.swing.JPanel;

import space.mcfish.mango.Core;
import space.mcfish.mango.ui.component.window.Window;
import space.mcfish.mango.ui.util.UI;
import space.mcfish.mango.ui.view.MenuView;
import space.mcfish.mango.ui.view.PaneView;

@SuppressWarnings("serial")
public class Renderer extends JPanel implements Runnable {

    public static boolean dragging = false;
    public static boolean rotating = false;

    public static View currentView = View.MENU;

    public static ArrayList<Window> windows;

    public static enum View {
        MENU, WORKSPACE, PANE
    };

    public Renderer() {
        windows = new ArrayList<Window>();
        new Thread(this).start();
    }

    /**
     * Draw call for the panel. Renders the panes of the active view to the JPanel.
     */
    @Override
    public void paint(Graphics g) {
        ((Graphics2D) g).setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
        switch (currentView) {
            case MENU:
                MenuView.draw((Graphics2D) g, (int) UI.WIDTH, (int) UI.HEIGHT);
                break;

            case WORKSPACE:
                g.setColor(UI.dullColor);
                g.fillRect(0, 0, (int) UI.WIDTH, (int) UI.HEIGHT);
                // iterate through windows in th workspace rendering them
                for (Window win : windows)
                    win.render((Graphics2D) g);

                break;

            case PANE:
                PaneView.draw((Graphics2D) g, (int) UI.WIDTH, (int) UI.HEIGHT);
                break;

            default:
                break;
        }
    }

    /*
     * Continually repaint self
     */
    public void run() {
        while (Core.running)
            repaint();
    }
}
