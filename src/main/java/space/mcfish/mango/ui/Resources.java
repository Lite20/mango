package space.mcfish.mango.ui;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.net.URISyntaxException;

import javax.imageio.ImageIO;

public class Resources {
    public String RES_DIRECTORY;

    public static BufferedImage OPEN_FILE, NEW_FILE, MANGO_LOGO;

    public Resources() throws URISyntaxException {
        RES_DIRECTORY = new File(Resources.class.getProtectionDomain().getCodeSource().getLocation().toURI())
                .getParent() + "\\";

        try {
            load();
        } catch (IOException e) {
            RES_DIRECTORY = "";
            try {
                load();
            } catch (IOException e1) {
                e1.printStackTrace();
            }
        }
    }

    private void load() throws IOException {
        OPEN_FILE = ImageIO.read(new File(RES_DIRECTORY + "res\\open_file.png"));
        NEW_FILE = ImageIO.read(new File(RES_DIRECTORY + "res\\save_file.png"));
        // MANGO_LOGO = ImageIO.read(new File(RES_DIRECTORY + "res\\mango_logo.png"));
    }
}
