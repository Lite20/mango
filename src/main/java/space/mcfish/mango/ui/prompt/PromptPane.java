package space.mcfish.mango.ui.prompt;

import java.awt.Color;
import java.awt.Graphics;

import javax.swing.JFrame;
import javax.swing.JPanel;

@SuppressWarnings("serial")
public class PromptPane extends JPanel {

    public JFrame window;

    public PromptPane(JFrame window) {
        this.window = window;
    }

    @Override
    public void paint(Graphics g) {
        g.setColor(Color.BLACK);
        g.fillRect(0, 0, window.getWidth(), window.getHeight());
    }
}
