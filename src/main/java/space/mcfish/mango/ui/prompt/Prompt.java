package space.mcfish.mango.ui.prompt;

import javax.swing.JFrame;

public class Prompt {
    public JFrame window;

    private PromptPane promptPane = new PromptPane(window);

    public Prompt(String title) {
        window = new JFrame(title);
        window.add(promptPane);
    }

    public void promptString(String label, PromptCallback callback) {

    }

    public void show() {
        window.setVisible(true);
    }

    public void hide() {
        window.setVisible(false);
    }

}
