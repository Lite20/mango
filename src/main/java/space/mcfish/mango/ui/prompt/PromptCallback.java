package space.mcfish.mango.ui.prompt;

public interface PromptCallback {
    public void handle();
}
