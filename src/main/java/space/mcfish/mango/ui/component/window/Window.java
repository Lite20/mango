package space.mcfish.mango.ui.component.window;

import java.awt.Graphics2D;
import java.awt.Rectangle;
import java.util.HashMap;
import java.util.Iterator;

import space.mcfish.mango.ui.component.UIComponent;
import space.mcfish.mango.ui.component.window.item.ControlBar;
import space.mcfish.mango.ui.util.DRect;
import space.mcfish.mango.ui.util.UI;

public class Window extends UIComponent {

    // the components in the window
    protected final HashMap<String, UIComponent> components;

    protected final ControlBar control_bar;

    public Window(DRect rect) {
        super(rect);
        control_bar = new ControlBar(rect);
        components = new HashMap<String, UIComponent>();
        components.put("mango.control_bar", control_bar);
    }

    /**
     * Draw the frame
     * 
     * @param g
     *            the graphics object to use to draw the frame
     */
    @Override
    public void render(Graphics2D g) {

        // draw components
        Iterator<UIComponent> itr = components.values().iterator();
        while (itr.hasNext())
            itr.next().render(g);

        // draw the corner drag handles handles
        g.setColor(UI.darkOmbre);
        g.drawLine(getX(), getY() + getHeight() - 14, getX() + 14, getY() + getHeight());
        g.drawLine(getX(), getY() + getHeight() - 10, getX() + 10, getY() + getHeight());
        g.drawLine(getX(), getY() + getHeight() - 6, getX() + 6, getY() + getHeight());

        g.setColor(UI.midOmbre);
        g.drawLine(getX() + getWidth() - 14, getY(), getX() + getWidth(), getY() + 14);
        g.drawLine(getX() + getWidth() - 10, getY(), getX() + getWidth(), getY() + 10);
        g.drawLine(getX() + getWidth() - 6, getY(), getX() + getWidth(), getY() + 6);

        g.setColor(UI.whiteOmbre);
        g.drawLine(getX(), getY() + getHeight() - 13, getX() + 13, getY() + getHeight());
        g.drawLine(getX(), getY() + getHeight() - 9, getX() + 9, getY() + getHeight());
        g.drawLine(getX(), getY() + getHeight() - 5, getX() + 5, getY() + getHeight());

        g.drawLine(getX() + getWidth() - 13, getY(), getX() + getWidth(), getY() + 13);
        g.drawLine(getX() + getWidth() - 9, getY(), getX() + getWidth(), getY() + 9);
        g.drawLine(getX() + getWidth() - 5, getY(), getX() + getWidth(), getY() + 5);

        // draw window bodrer
        g.setColor(UI.lightOmbre);
        g.drawLine(getX() + getWidth(), getY(), getX(), getY());
        g.drawLine(getX() + getWidth(), getY() + getHeight() - UI.BAR_HEIGHT, getX(),
                getY() + getHeight() - UI.BAR_HEIGHT);

        g.setColor(UI.midOmbre);
        g.drawLine(getX() + getWidth(), getY(), getX() + getWidth(), getY() + getHeight());
        g.drawLine(getX() + getWidth(), getY() + getHeight() - UI.BAR_HEIGHT - 1, getX(),
                getY() + getHeight() - UI.BAR_HEIGHT - 1);

        g.setColor(UI.darkOmbre);
        g.drawLine(getX(), getY(), getX(), getY() + getHeight());
        g.drawLine(getX(), getY() + getHeight(), getX() + getWidth(), getY() + getHeight());
    }

    public Rectangle getBounds() {
        return new Rectangle(getX(), getY(), getWidth(), getHeight());
    }
}
