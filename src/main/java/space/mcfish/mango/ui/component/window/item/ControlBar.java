package space.mcfish.mango.ui.component.window.item;

import java.awt.Color;
import java.awt.Graphics2D;

import space.mcfish.mango.ui.component.UIComponent;
import space.mcfish.mango.ui.util.DRect;
import space.mcfish.mango.ui.util.UI;

public class ControlBar extends UIComponent {

    public DRect container;

    public ControlBar(DRect containerDimensions) {
        super(new DRect(containerDimensions, 0f, 1f, 1f, 0f));
        this.container = containerDimensions;
        this.rect.y.patch(-UI.BAR_HEIGHT);
        this.rect.height.patch(UI.BAR_HEIGHT);
    }

    @Override
    public void render(Graphics2D g) {
        g.setColor(Color.RED);
        g.fillRect(getX(), getY(), getWidth(), getHeight());
    }
}
