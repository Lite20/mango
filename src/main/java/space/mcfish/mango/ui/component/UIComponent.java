package space.mcfish.mango.ui.component;

import java.awt.Graphics2D;

import space.mcfish.mango.ui.util.DRect;

// TODO let everything be anchored and just have some be dynamic while others be fixed
public class UIComponent {

    protected final DRect rect;

    public UIComponent(DRect rect) {
        this.rect = rect;
    }

    public void render(Graphics2D g) {

    }

    public void onPointer(int deviceType, int pointerID, int eType, boolean inverted, int x, int y, int pressure) {

    }

    public int getX() {
        return (int) rect.x.get();
    }

    public int getY() {
        return (int) rect.y.get();
    }

    public int getWidth() {
        return (int) rect.width.get();
    }

    public int getHeight() {
        return (int) rect.height.get();
    }
}
