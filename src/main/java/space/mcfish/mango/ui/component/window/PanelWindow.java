package space.mcfish.mango.ui.component.window;

import java.awt.Color;
import java.awt.Graphics2D;

import space.mcfish.mango.Core;
import space.mcfish.mango.doc.Pane;
import space.mcfish.mango.ui.util.DRect;
import space.mcfish.mango.ui.util.UI;

public class PanelWindow extends Window {

    public PanelWindow(DRect dims) {
        super(dims);
    }

    @Override
    public void render(Graphics2D g) {
        if (Core.getCurrentPage() == null)
            return;

        if (Core.getCurrentPage().paneCount() == 0) {
            // TODO optimize this rapidly made junk
            String notice = "Press `<CTRL> + N` to make a new pane";
            g.setPaint(Color.BLACK);
            g.setFont(UI.defaultFont);
            int w = g.getFontMetrics(UI.defaultFont).stringWidth(notice);
            g.drawString(notice, getX() + (getWidth() / 2) - (w / 2), getY() + (getHeight() / 2) - 6);
        } else {
            for (int x = 0; x < Core.getCurrentPage().paneCount(); x++) {
                Pane pane = Core.getCurrentPage().panes.get(x);
                g.setColor((x == Core.getCurrentPage().activePaneIndex) ? Color.BLUE : UI.lineColor);
                g.setStroke(UI.borderStroke);
                g.drawImage(pane.buffer, (int) getX() + Core.getCurrentPage().xScrollOffset + (int) pane.x,
                        (int) getY() + Core.getCurrentPage().yScrollOffset + (int) pane.y, (int) pane.w, (int) pane.h,
                        null);

                g.drawRect((int) getX() + Core.getCurrentPage().xScrollOffset + (int) pane.x,
                        (int) getY() + Core.getCurrentPage().yScrollOffset + (int) pane.y, (int) pane.w, (int) pane.h);
            }
        }

        super.render(g);
    }

    @Override
    public void onPointer(int deviceType, int pointerID, int eType, boolean inverted, int x, int y, int pressure) {
        // TODO Auto-generated method stub
    }
}
