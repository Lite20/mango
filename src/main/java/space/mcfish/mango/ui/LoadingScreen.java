package space.mcfish.mango.ui;

import java.awt.Color;
import java.awt.Graphics;

import javax.swing.JPanel;

public class LoadingScreen extends JPanel implements Runnable {
    private static final long serialVersionUID = -3226867990372027358L;

    public boolean loading;

    public LoadingScreen() {
        loading = true;
        new Thread(this).start();
    }

    @Override
    public void paint(Graphics g) {
        g.setColor(Color.WHITE);
        g.fillRect(0, 0, WIDTH, HEIGHT);
    }

    public void run() {
        while (loading) {
            repaint();
        }
    }
}
