package space.mcfish.mango.ui.util;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Stroke;

import javax.swing.JFrame;
import javax.swing.JMenuBar;

import space.mcfish.mango.Core;
import space.mcfish.mango.events.EventHandler;
import space.mcfish.mango.ui.LoadingScreen;
import space.mcfish.mango.ui.Renderer;
import space.mcfish.mango.ui.menubar.EditMenu;
import space.mcfish.mango.ui.menubar.FileMenu;
import space.mcfish.mango.ui.menubar.HelpMenu;

public class UI {
    public static final Color dullColor = new Color(0xF2F2F2);
    public static final Color lineColor = new Color(0xE5E5E5);

    public static final Font defaultFont = new Font("Aerial", Font.PLAIN, 12);

    public static final Stroke stroke = new BasicStroke(3);
    public static final Stroke borderStroke = new BasicStroke(1);

    public static final BasicStroke selectorStroke = new BasicStroke(1.0f);

    public static final Color barOmbre = new Color(0, 0, 0, 50);
    public static final Color whiteOmbre = new Color(250, 250, 250, 175);
    public static final Color lightOmbre = new Color(200, 200, 200, 175);
    public static final Color midOmbre = new Color(100, 100, 100, 150);
    public static final Color darkOmbre = new Color(0, 0, 0, 175);

    public static final int BAR_HEIGHT = 27;

    public static int WIDTH;
    public static int HEIGHT;

    public static JFrame createEditor(Renderer panel, EventHandler eHandle, boolean fullscreen) {
        // build window
        JFrame window = new JFrame("Mango");

        // build menu bar
        JMenuBar menuBar = new JMenuBar();

        FileMenu.bake(menuBar);
        EditMenu.bake(menuBar);
        HelpMenu.bake(menuBar);

        // compute offset of main panel
        Core.yPad += menuBar.getHeight();

        // add panel and menu bar
        window.add(panel);
        window.setJMenuBar(menuBar);

        // adding listeners
        window.addKeyListener(eHandle);
        window.addComponentListener(eHandle);
        window.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        // fullscreen support
        if (fullscreen) {
            window.setExtendedState(JFrame.MAXIMIZED_BOTH);
            window.setUndecorated(true);
        }

        // set scale and location
        window.setPreferredSize(new Dimension(1280, 720));
        window.pack();
        window.setLocationRelativeTo(null);

        return window;
    }

    public static JFrame createLoader(LoadingScreen lp) {
        // create window
        JFrame window = new JFrame("Mango");

        // add loading pane
        window.add(lp);

        // remove window decorations
        window.setUndecorated(true);

        // set window size and location
        window.setPreferredSize(new Dimension(480, 240));
        window.pack();
        window.setLocationRelativeTo(null);

        return window;
    }

    public static void recomputeSize() {
        if (Core.w != null) {
            WIDTH = (int) Core.w.getContentPane().getSize().getWidth();
            HEIGHT = (int) Core.w.getContentPane().getSize().getHeight();
        }
    }
}
