package space.mcfish.mango.ui.util;

import space.mcfish.mango.Core;

public class DInt {
    public static int X_AXIS = 0;
    public static int Y_AXIS = 1;

    private DInt mult;

    // adjustment allows fixed values to work
    private int adjustment = 0;

    int axis;

    float val;

    public DInt(float val, DInt mult) {
        this.mult = mult;
        this.val = val;
    }

    public DInt(float val, int axis) {
        this.mult = null;
        this.val = val;
        this.axis = axis;
    }

    public float get() {
        if (mult != null)
            return (val * mult.get()) + (float) adjustment;

        else {
            if (this.axis == X_AXIS)
                return ((float) Core.w.getContentPane().getWidth() * val) + (float) adjustment;

            else
                return ((float) Core.w.getContentPane().getHeight() * val) + (float) adjustment;
        }
    }

    // we use patch to adjust the fixed value
    public void patch(int p) {
        this.adjustment += p;
    }
}
