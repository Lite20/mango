package space.mcfish.mango.ui.util;

public class DRect {
    public final DInt x;
    public final DInt y;
    public final DInt width;
    public final DInt height;

    public DRect(DInt x, DInt y, DInt w, DInt h) {
        this.width = w;
        this.height = h;
        this.x = x;
        this.y = y;
    }

    public DRect(DRect parent, float x, float y, float w, float h) {
        this.x = new DInt(x, parent.width);
        this.y = new DInt(y, parent.height);
        this.width = new DInt(w, parent.width);
        this.height = new DInt(h, parent.height);
    }
}
